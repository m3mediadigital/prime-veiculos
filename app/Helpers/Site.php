<?php

namespace App\Helpers;

class Site{

    public static function unidades($settings)
    {
        $_units = [
            'zona_sul'    => [
                'nome' => 'Zona Sul - Aracaju/SE',
                'connect1' => 'zona_sul',
                'connect2' => 'riomar',
                'endereco' => '',
                'telefone' => '',
                'horarios' => '',
            ],
            'rotary_club'  => [
                'nome' => 'Rotary Club - Itabaiana/SE',
                'connect1' => 'rotary_club',
                'connect2' => 'itabaiana',
                'endereco' => '',
                'telefone' => '',
                'horarios' => '',
            ],
            'st_antonio' => [
                'nome' => 'Santo Antônio - Aracaju/SE',
                'connect1' => 'st_antonio',
                'connect2' => 'st_antonio',
                'endereco' => '',
                'telefone' => '',
                'horarios' => '',
            ]
        ];

        
        foreach ($_units as $key => $value) {
            $_units[$key]['endereco'] = (isset($settings['endereco_' . $value['connect1']])) ? $settings['endereco_' . $value['connect1']] : $settings['endereco_' . $value['connect2']];
            $_units[$key]['telefone'] = (isset($settings['tel_' . $value['connect1']])) ? $settings['tel_' . $value['connect1']] : $settings['tel_' . $value['connect2']];
            $_units[$key]['horarios'] = (isset($settings['horario_footer_' . $value['connect1']])) ? self::formataHorarioUnidade($settings['horario_footer_' . $value['connect1']]) : self::formataHorarioUnidade($settings['horario_footer_' . $value['connect2']]);
        }
       
        return $_units;
    }

    private static function formataHorarioUnidade($horario)
    {
        $h2 = explode(" | ", $horario);
        $horarios = '';
        foreach ($h2 as $v) {
            $horarios .= '<p class="m-0">' . $v . '</p>';
        }

        return $horarios; 
    }

    public static function formatarPreco($valor)
    {
        $price = null;
        if (!is_null($valor) && $valor != "0.00"){
            $v =  explode('.', $valor);
            $int = number_format($v[0],0,',','.');
            $dec = $v[1];
            $price = [$int, $dec];
        }
        
        return $price;
    }


    private static function priceByColumn()
    {
        $types = [
            'Expressao' => ['custom_value', 'custom_subvalue'],
            'DescontoDe' => ['discount'],
            'DePor' => ['value_from', 'value_to'],
            'APartirDe' => ['value_to'],
            'Bonus' => ['bonus'],
            'EntradaParcelas' => ['parcel_value', 'parcel_amount', 'entry'],
            'ApenasPor' => ['used_value'],
        ];

        return $types;
    }

    public static function getTypePrice($item)
    {
        $aTypes = self::priceByColumn();
        foreach ($aTypes as $type => $fields) {
            $group = [];
            $values = [];
            foreach ($fields as $k => $field) {
                if (isset($item->{$field}) && !is_null($item->{$field})){
                    $group[] = $field;
                    $values[$field] = $item->{$field};
                }
            }
            if (in_array($group, $aTypes)){
                if ($type == 'DePor' && !isset($values['value_from'])){
                    $type = 'APartirDe';
                }

                if ($item->payment_method == 'parcel_no_entry'){
                    $type = 'EntradaZero';
                }
                return $typePrice = [
                    'type' => $type, 
                    'values' => $values
                ];
            }
        }
    }

}