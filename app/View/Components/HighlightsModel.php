<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\View;

class HighlightsModel extends Component
{
    public $item;

    public function __construct($item)
    {
        $this->item = $item;
        view()->share('count', $this->item->highlightsModel->count() - 1);
    }

    public function render()
    {
        return view('components.highlights-model');
    }
}
