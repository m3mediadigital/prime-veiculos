<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Price extends Component
{
    public $values;
    public $typePrice;

    public function __construct($values, $typePrice)
    {
        $this->values = $values;
        $this->typePrice = $typePrice;
    }

    public function render()
    {
        return view('components.price');
    }
}
