<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PrecoEntradaParcelas extends Component
{
    public $valores;

    public function __construct($valores)
    {
        $this->valores = [
            'parcel_value' => \App\Helpers\Site::formatarPreco($valores['parcel_value']), 
            'parcel_amount' => $valores['parcel_amount'], 
            'entry' => \App\Helpers\Site::formatarPreco($valores['entry'])
        ];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.preco-entrada-parcelas');
    }
}
