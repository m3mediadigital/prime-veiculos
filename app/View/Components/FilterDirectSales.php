<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FilterDirectSales extends Component
{
    public $showroom;
    public function __construct($showroom)
    {
        $a = [];

        foreach($showroom as $item):
            $a [] = (string)str_replace('_',' ',$item->corporate_sales_category);
        endforeach;

        $this->showroom = array_unique($a);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.filter-direct-sales');
    }
}
