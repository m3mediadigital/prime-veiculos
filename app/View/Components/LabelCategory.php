<?php

namespace App\View\Components;

use Illuminate\View\Component;

class LabelCategory extends Component
{
    
    public $category;
    
    
    public function __construct($category = null)
    {
        $this->category = $category;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.label-category');
    }
}
