<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardOffer extends Component
{
    public $item;
    public $priceValues;
    public $typePrice;

    public function __construct($item)
    {
        $this->item = $item;

        $aTypes = \App\Helpers\Site::getTypePrice($item);
        $this->priceValues = $aTypes['values'];
        $this->typePrice = $aTypes['type'];
    }

    public function render()
    {
        return view('components.card-offer');
    }
}
