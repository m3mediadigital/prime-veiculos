<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PrecoAPartirDe extends Component
{
    public $valor;

    public function __construct($valor)
    {
        $this->valor = \App\Helpers\Site::formatarPreco($valor['value_to']);
    }

    public function render()
    {
        return view('components.preco-a-partir-de');
    }
}
