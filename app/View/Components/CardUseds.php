<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardUseds extends Component
{
    public $item;
    public $typePrice;
    public $priceValues;

    public function __construct($item)
    {
        $this->item = $item;

        $aTypes = \App\Helpers\Site::getTypePrice($item);
        $this->priceValues = $aTypes['values'];
        $this->typePrice = $aTypes['type'];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.card-useds');
    }
}
