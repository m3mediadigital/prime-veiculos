<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PrecoBonusDe extends Component
{
    public $valor;

    public function __construct($valor)
    {
        $this->valor = \App\Helpers\Site::formatarPreco($valor['valor']);
    }

    
    public function render()
    {
        return view('components.preco-bonus-de');
    }
}
