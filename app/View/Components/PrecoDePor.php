<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PrecoDePor extends Component
{
    public $valores;

    public function __construct($valores)
    {
        $this->valores = [
            'value_from' => \App\Helpers\Site::formatarPreco($valores['value_from']), 
            'value_to' => \App\Helpers\Site::formatarPreco($valores['value_to'])
        ];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.preco-de-por');
    }
}
