<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PrecoPorApenas extends Component
{
    public $valor;

    public function __construct($valor)
    {
        $this->valor = \App\Helpers\Site::formatarPreco($valor['used_value']);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.preco-por-apenas');
    }
}
