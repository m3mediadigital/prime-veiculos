<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Versions extends Component
{
    public $item;
    public $name;

    public function __construct($item, $name)
    {
        $this->item = $item;
        $this->name = $name;

        // dd($item);
        // foreach($item as $i){
        //     dd($i);
        // }
    }

    public function render()
    {
        return view('components.versions');
    }
}
