<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardDirectSales extends Component
{
    public $item;
    public $typePrice;
    public $priceValues;

    public function __construct($item, $typePrice = null, $priceValues = null)
    {
        $this->item = $item;
        $this->priceValues = self::getPrice($item);
        $this->typePrice = self::getTypePrice($item);
    }

    public function render()
    {
        return view('components.card-direct-sales');
    }

    private static function getPrice($item)
    {
        $priceValues = (isset($item->discount) && !is_null($item->discount)) ? $priceValues = ['valor' => $item->discount] : ['valor' => $item->value_to];
        return $priceValues;
    }

    private static function getTypePrice($item)
    {
        $typePrice = (isset($item->discount) && !is_null($item->discount)) ? $typePrice = 'DescontoDe' : 'PorApenas';
        return $typePrice;
    }
}
