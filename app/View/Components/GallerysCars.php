<?php

namespace App\View\Components;

use Illuminate\View\Component;

class GallerysCars extends Component
{
    public $item;
    public $name;

    public function __construct($item, $name)
    {
        $this->item = $item;
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.gallerys-cars');
    }
}
