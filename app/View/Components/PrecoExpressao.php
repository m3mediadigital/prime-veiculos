<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PrecoExpressao extends Component
{
    public $valores;

    public function __construct($valores)
    {
        $this->valores = $valores;
    }

    public function render()
    {
        return view('components.preco-expressao');
    }
}
