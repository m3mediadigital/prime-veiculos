<?php

namespace App\View\Components;

use Illuminate\View\Component;

class LabelWidget extends Component
{
    public $widgets;

    public function __construct($widgets = null)
    {
        $this->widgets = isset($widgets) ? $widgets : null;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.label-widget');
    }
}
