<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PrecoDescontoDe extends Component
{
    
    public $valor;    

    public function __construct($valor)
    {
        $this->valor = self::formatarDesconto($valor['valor']);
    }

    public function render()
    {
        return view('components.preco-desconto-de');
    }

    private static function formatarDesconto($valor)
    {
        $discount = trim(substr($valor, 0, -1));
        return $discount;
    }
}
