<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use App\Models\Slide;
use App\Models\Setting;
use App\Models\Showroom;

use App\Helpers\Site;

class PagesController extends Controller
{

    public $settings;  

    public function __construct()
    {
        $this->settings = Setting::customFetchAll();
        
        View::share('units', Site::unidades($this->settings));
        View::share('settings', $this->settings);
    }

    public function index()
    {
        return view('pages.index',[
            'slides' => Slide::allSlidesFetch(),
            'offers' => Showroom::allOffers(6),
            'useds' => Showroom::allUsedsFetch(3)
        ]);
    }

    public function offers()
    {
        // dump(Showroom::allOffers(null));
        return view('pages.offers',[
            'offers' => Showroom::allOffers(null)
        ]);
    }

    public function offer($slug)
    {
        return view('pages.offers',[
            'offers' => Showroom::allOffers(null)
        ]);
    }

    public function directSales()
    {
        return view('pages.directsales',[
            'directSales' => Showroom::allDirectSales()
        ]);
    }

    public function news()
    {
        return view('pages.news',[
            'news' => Showroom::allNewsList()
        ]);
    }

    public function new($slug)
    {
        return view('pages.new',[
            'item' => Showroom::allNewsFetch($slug)
        ]);
    }

    public function useds()
    {
        return view('pages.useds', [
            'useds' => Showroom::allUsedsFetch()
        ]);
    }

    public function used()
    {
        return view('pages.used');
    }

    public function pcds()
    {
        return view('pages.pcds', [
            'pcds' => Showroom::allDirectSales('pcd')
        ]);
    }

    public function scheduling()
    {
        return view('pages.scheduling');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function financing()
    {
        return view('pages.financing');
    }
}
