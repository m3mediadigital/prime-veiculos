<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class Showroom extends AppDescomplicarModel
{
    protected $table = 'showroom';

    public function company()
    {
        return $this->belongsTo('\App\Models\Company', 'companies_id', 'id');
    }

    public function model()
    {
        return $this->belongsTo('\App\Models\Model', 'models_id', 'id');
    }

    public function version()
    {
        return $this->belongsTo('\App\Models\Version', 'versions_id', 'id');
    }

    public function color()
    {
        return $this->belongsTo('\App\Models\Color', 'colors_id', 'id');
    }

    public function coupon()
    {
        return $this->hasMany('\App\Models\Coupon');
    }

    public function custom_answer()
    {
        return $this->hasMany('\App\Models\CustomAnswer', 'showroom_id', 'id');
    }

    public function showroom_visit()
    {
        return $this->hasMany('\App\Models\ShowroomVisit');
    }

    public function versoes()
	{
	    return $this->belongsToMany('App\Models\Version', 'showroom_has_versions', 'showroom_id', 'versions_id');
	}

    public function optionals()
	{
	    return $this->belongsToMany('App\Models\Optional', 'showroom_has_optionals', 'showroom_id', 'optionals_id');
    }
    
    //init News
    public static function allNewsList()
    {
        $query = Self::select(
                'id' ,
                'models_id',
                'slug'
        )->with([
            'model' => function($q){
                $q->select(
                    'id',
                    'name',
                    'complete_name',
                    DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image")
                );
            }
        ]);

        $query = $query->where([
                ['type','novo'],
                ['active', '1'],
                ['companies_id', self::$COMPANY_ID]
        ])->get();

        return $query;
    }

    public static function allNewsFetch($slug)
    {
        $query = Self::select(
                'id' ,
                'models_id',
                'slug'
        );

        self::containShowroomNews($query);

        $query = $query->where([
                ['type','novo'],
                ['active', '1'],
                ['slug', $slug],
                ['companies_id', self::$COMPANY_ID]
        ])->first();

        // dd($query);

        return $query;
    }

    private static function containShowroomNews(&$query)
    {
        $query->with([
            'model' => function($q){
                $q->select(
                    'id',
                    'name',
                    'complete_name',
                    DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image")
                )
                ->with([
                    'highlightsModel' => function($q){
                        $q->select(
                            'id',
                            'name',
                            DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image"),
                            'content',
                            'position',
                            'subtitle',
                            'models_id'
                        );
                    },
                    'modelGallery' => function($q){
                        $q->select(
                            'models_id',
                            'path'
                        );
                    },
                    'versions' => function($q){
                        $q->select(
                            'id',
                            'models_id',
                            'slug',
                            'name',
                            'complete_name',
                            DB::raw("CONCAT('". self::$IMAGE_URI . "', large_image) as large_image")
                        )
                        ->with([
                            'colors' => function($q){
                                $q->select(
                                    'id',
                                    'color',
                                    'hexadecimal',
                                    'versions_id',
                                    DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image")
                                );
                            },
                            'optionals' => function($q){
                                $q->select(
                                    'versions_id', 
                                    'optionals_id',
                                    'value'
                                );
                            }
                        ]);
                    } 
                ]);
            },
        ]);
    }

    //end News

    // Init Offers

    public static function allOffers($limit = null)
    {
        $query = self::query()
        ->select(
            'id',
            'slug',
            'description',
            'payment_method',
            'colors_id',
            'parcel_amount',
            'parcel_value',
            'entry',
            'bonus',
            'value_from',
            'value_to',
            'finish_at',
            'type',
            'models_id',
            'versions_id'
        )->with([
            'model' => function($q){
                $q->select(
                    'id',
                    'name',
                    'year',
                    DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image")
                );
            },
            'version' => function($q){
                $q->select(
                    'id',
                    'complete_name',
                    DB::raw("CONCAT('". self::$IMAGE_URI . "', large_image) as large_image")
                );
            },
            'color' => function($q) {
                $q->select(
                    'id',
                    'color',
                    DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image")
                );
            },
            'custom_answer' => function($q) {
                $q->with([
                    'custom_question' => function($q) {
                        $q->select(
                            'id',
                            'slug',
                            'type'
                        );
                    }
                ]);
            }
        ]);

        $query = $query->where([
            ['companies_id', self::$COMPANY_ID],
            ['type', 'oferta'],
            ['active', '1'],
            ['finish_at', '>=', date('Y-m-d')],
        ]);
            
        $query = $limit !== null ? $query->limit($limit)->inRandomOrder() : $query->inRandomOrder();
        
        $cars = $query->get();

        foreach($cars as $k => $car) {
            $car = self::singleCustomKey($car);
            if($car->has('optionals')) {
                unset($car->optionals);
            }
            $cars[$k] = $car;
        }
        // dump($cars);
        return $cars;
    }


    // End Offers

    private static function containCustomAnswer(&$query)
    {
        $query->with([
            'custom_answer' => function($q) {
                $q->select(
                    'id',
                    'showroom_id',
                    'custom_questions_id',
                    'value'
                )
                ->with([ 'custom_question' => function($q) {
                        $q->select([
                            'id',
                            'slug',
                            'type'
                        ]);
                    }
                ]);
            }
        ]);
    }

    //Direct Sales

    public static function listDirectSales()
    {
        $query = Self::select(
            'id',
            'slug',
            'discount',
            'value_from',
            'models_id',
            'colors_id',
            'versions_id',
            'corporate_sales_category'
        )
        ->with([
            'model' => function($q){
                $q->select('id','name');
            },
            'color' => function($q){
                $q->select('id',DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image"));
            },
            'version' => function($q){
                $q->select(
                    'id',
                    'complete_name',
                    DB::raw("CONCAT('". self::$IMAGE_URI . "', large_image) as large_image")
                );
            }
        ]);

        $cars = $query->where([
                'type' => 'vendas_diretas',
                'active' => '1',
                'companies_id' => self::$COMPANY_ID,
            ])->orderBy('corporate_sales_category', 'ASC')->get();


        return $cars;

    }


    public static function allDirectSales($category = null)
    {
        $query = Self::select(
            'id',
            'slug',
            'discount',
            'value_from',
            'models_id',
            'colors_id',
            'versions_id',
            'corporate_sales_category'
        )
        ->with([
            'model' => function($q){
                $q->select('id','name');
            },
            'color' => function($q){
                $q->select('id',DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image"));
            },
            'version' => function($q){
                $q->select(
                    'id',
                    'complete_name',
                    DB::raw("CONCAT('". self::$IMAGE_URI . "', large_image) as large_image")
                );
            }
        ]);

        self::containCustomAnswer($query);
        
        $query = $category !== null ? $query->where('corporate_sales_category', $category)->inRandomOrder() : $query->inRandomOrder();
        
        $cars = $query->where([
                'type' => 'vendas_diretas',
                'active' => '1',
                'companies_id' => self::$COMPANY_ID,
        ])
        ->orderBy('corporate_sales_category', 'ASC')->get();
        
        foreach($cars as $k => $car) {
            $car = self::singleCustomKey($car);
            if($car->has('optionals')) {
                unset($car->optionals);
            }
            $cars[$k] = $car;
        }

        return $cars;

    }

    public static function allUsedsFetch($limit = null)
    {
        $query = Self::select(
            'id',
            'slug',
            'used_value',
            'fab_year',
            'model_year',
            'payment_method',
            'models_id',
            'versions_id'
        )
        ->with([
            'model' => function($q){
                $q->select('id','complete_name');
            },
            'version' => function($q){
                $q->select(
                    'id',
                    'complete_name'
                );
            }
        ]);

        self::containCustomAnswer($query);

        $query = $limit !== null ? $query->limit($limit)->inRandomOrder() : $query->inRandomOrder();
        
        $cars = $query->where([
            'type' => 'seminovo',
            'active' => '1',
            'companies_id' => self::$COMPANY_ID,
        ])
        ->orderBy('id', 'DESC')->get();

        foreach($cars as $k => $car) {
            $car = self::singleCustomKey($car);
            if($car->has('optionals')) {
                unset($car->optionals);
            }
            $cars[$k] = $car;
        }

        return $cars;

    }

}
