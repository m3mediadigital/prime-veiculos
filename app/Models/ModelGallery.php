<?php

namespace App\Models;

class ModelGallery extends AppDescomplicarModel
{
    protected $table = 'models_gallery';

    public function model()
    {
        return $this->belongsTo('App\Models\Model');
    }

}
