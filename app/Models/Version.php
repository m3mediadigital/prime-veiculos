<?php

namespace App\Models;


class Version extends AppDescomplicarModel
{
    public function model()
    {
        return $this->belongsTo('App\Models\Model', 'models_id', 'id');
    }

    public function colors()
    {
        return $this->hasMany('App\Models\Color', 'versions_id');
    }

    public function optionals()
    {
        return $this->belongsToMany('App\Models\Optional', 'versions_has_optionals', 'versions_id', 'optionals_id');
    }
}
