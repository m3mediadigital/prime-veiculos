<?php

namespace App\Models;

class Model extends AppDescomplicarModel
{
    public function brand()
    {
	    return $this->belongsTo('App\Models\Brand', 'brands_id', 'id');
    }

    public function type()
    {
	    return $this->belongsTo('App\Models\Type', 'type_id', 'id');
    }

     public function versions()
    {
	    return $this->hasMany('App\Models\Version', 'models_id');
    }

    public function highlightsModel()
    {
        return $this->hasMany('App\Models\HighlightsModel', 'models_id');
    }

    public function modelGallery()
    {
        return $this->hasMany('App\Models\ModelGallery', 'models_id');
    }

    public function colors()
	{
		return $this->hasMany('App\Models\Color', 'models_id', 'id');
    }
}
