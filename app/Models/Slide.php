<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class Slide extends AppDescomplicarModel
{

    public function showroom()
	{
	    return $this->belongsTo('App\Models\Showroom', 'showroom_id', 'id');
	}

    public static function allSlidesFetch()
    {
        $slides = self::query()
            ->select(
                'id',
                'link',
                DB::raw("CONCAT('". self::$IMAGE_URI . "', image) as image")
            )->where([
                'companies_id' => self::$COMPANY_ID,
                'active' => self::ACTIVE,
                ['start_at', '<=', date('Y-m-d')],
                ['finish_at', '>=', date('Y-m-d')],
            ])->orderBy('position', 'ASC')
            ->get();
       
        return $slides;
    }
}
