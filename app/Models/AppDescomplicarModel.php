<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppDescomplicarModel extends Model
{
    const ACTIVE = 1;
    
	static $COMPANY_ID = 19;
    static $COMPANY_KEY = '29300a53e34fa947a29b0058f2d444ca';
    static $IMAGE_URI = 'https://descomplicar.s3-sa-east-1.amazonaws.com/upload/';

    
    protected static function singleCustomKey(&$carro)
    {
        if($carro && !empty($carro->custom_answer)) {
            foreach($carro->custom_answer as $k => $v) {
                $campo = $v->custom_question->slug;

                if($v->custom_question->type == 'select') {
                    $option = CustomOption::query()
                        ->select([
                            'custom_option'
                        ])
                        ->where([
                            'custom_questions_id' => $v->custom_question->id,
                            'id' => $v->value,
                        ])->first();

                    $carro->$campo = $option->custom_option;

                } else if($v->custom_question->type == 'checkbox') {
                    $option = CustomOption::query()
                        ->select('custom_option')
                        ->where('custom_questions_id', $v->custom_question->id)
                        ->whereIn('id', unserialize($v->value))
                        ->get();

                    $opts = array();

                    foreach($option as $opt) {
                        $opts[] = $opt->custom_option;
                    }
                    $carro->$campo = $opts;

                } else if(in_array($v->custom_question->type, ['image', 'album_images']) && trim($v->value) != '') {
                    $carro->$campo = self::$IMAGE_URI . $v->value;

                } else if($v->custom_question->type == 'money') {
                    $money = str_replace('.', '', $v->value);
                    $money = str_replace(',', '.', $money);
                    $carro->$campo = (float)$money;

                } else {
                    $carro->$campo = $v->value;
                }

            }
            unset($carro->custom_answer);
        }

        $optionals_array = [];

        if($carro && !empty($carro->optionals)) {
            foreach($carro->optionals as $k => $opt) {
                $carro->optionals[$k]->slug = mb_convert_case( preg_replace('/(\s+)/', '-', $opt->value), MB_CASE_LOWER, 'utf-8' );
                $optionals_array[ $carro->optionals[$k]->slug ] = $opt->value;
            }
            unset($carro->optionals);
        }
        $carro->optionals = $optionals_array;

        return $carro;
    }

}