<?php

namespace App\Models;


class CustomAnswer extends AppDescomplicarModel
{
    protected $table = 'custom_answer';

    public function custom_question()
    {
        return $this->belongsTo('App\Models\CustomQuestion', 'custom_questions_id', 'id');
    }
}
