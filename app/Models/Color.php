<?php

namespace App\Models;

class Color extends AppDescomplicarModel
{
    public function model()
    {   
        return $this->belongsTo('App\Models\Model', 'models_id', 'id');
    }

    public function version()
    {   
        return $this->belongsTo('App\Models\Version', 'versions_id', 'id');
    }
}
