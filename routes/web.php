<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('index');
Route::get('ofertas', 'PagesController@offers')->name('offers');
Route::get('oferta/{slug}', 'PagesController@offer')->name('offer');
Route::get('vendas-diretas', 'PagesController@directSales')->name('directsales');
Route::get('venda-direta/{slug}', 'PagesController@directSale')->name('directsale');
Route::get('showroom', 'PagesController@news')->name('news');
Route::get('showroom/{slug}', 'PagesController@new')->name('new');
Route::get('seminovos', 'PagesController@useds')->name('useds');
Route::get('seminovo', 'PagesController@used')->name('used');
Route::get('pcds', 'PagesController@pcds')->name('pcds');
Route::get('pcd/{slug}', 'PagesController@pcd')->name('pcd');
Route::get('fale-conosco', 'PagesController@contact')->name('contact');
Route::get('agendamento', 'PagesController@scheduling')->name('scheduling');
Route::get('quem-somos', 'PagesController@about')->name('about');
Route::get('financiamento', 'PagesController@financing')->name('financing');