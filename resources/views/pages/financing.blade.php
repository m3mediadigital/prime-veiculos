@extends('layouts.default', ['title' => 'Financiamento'])

@section('content')
      <section class="news offers pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offers-title text-center text-title mb-0 text-capitalize">financiamento</h1>
            <p class="text-center pb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. <br> Placeat minima voluptas aperiam optio distinctio 
                harum esse!</p>
            <form class="pt-1 forms pb-5">
                <div class="form-row">
                    @for($i = 0; $i < 10; $i++)
                        <x-CardFinancing />
                    @endfor
                </div>
                <div class="form-row">
                    <div class="form-group col-12 col-md-7">
                        @error('name') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <input type="text" class="form-control text-uppercase @error('name') is-invalid @enderror" placeholder=" " name="name" value="{{ old('name') }}">
                        <label for="name" class="text-uppercase">nome completo</label>                    
                    </div>
                    <div class="form-group col-12 col-md-5">
                        @error('phone') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <input type="tel" class="form-control text-uppercase sp_celphones @error('phone') is-invalid @enderror" placeholder=" " name="phone" value="{{ old('phone') }}">
                        <label for="name" class="text-uppercase">telefone</label>                    
                    </div>
                    <div class="form-group col-12 col-md-8">
                        @error('email') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <input type="email" class="form-control text-uppercase @error('email') is-invalid @enderror" placeholder=" " name="email" value="{{ old('email') }}">
                        <label for="name" class="text-uppercase">e-mail</label>                    
                    </div>
                    <div class="form-group col-12 col-md-4">
                        @error('loja') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <select class="form-control text-uppercase @error('loja') is-invalid @enderror" placeholder=" " name="loja">
                            <option value="">Loja</option>
                            <option value="{{ old('loja') }}">Loja</option>
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        @error('model') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <select class="form-control text-uppercase @error('model') is-invalid @enderror" placeholder=" " name="model">
                            <option value="">incluir emplacamento</option>
                            <option value="{{ old('model') }}">model</option>
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        @error('input') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <input type="text" class="form-control text-uppercase @error('input') is-invalid @enderror" placeholder=" " name="input" value="{{ old('input') }}">
                        <label for="name" class="text-uppercase">valor de entrada</label>   
                    </div>
                    <div class="form-group col-12 col-md-4">
                        @error('parcel') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <select class="form-control text-uppercase @error('parcel') is-invalid @enderror" placeholder=" " name="parcel">
                            <option value="">parcelas</option>
                            <option value="{{ old('parcel') }}">model</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 d-flex justify-content-center pt-5">
                    <button type="submit" class="btn btn-primary border-0 rounded-0 pr-5 pl-5 text-uppercase">enviar</button>
                </div>
            </form>
        </div>
    </section>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>
    <script>
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

        $('.sp_celphones').mask(SPMaskBehavior, spOptions);
    </script>
@endpush