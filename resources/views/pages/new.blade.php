@extends('layouts.default', ['title' => $item->model->complete_name])
{{-- {{ dd($item->model->highlightsModel[0]) }} --}}
@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
@endpush

@section('content')
    <section class="new">
        <div class="header w-100" style="background-image: linear-gradient(rgba(0,0,0,.28), rgba(0,0,0,.28)), url({{ $item->model->highlightsModel[0]->image }})">
            <div class="container text-center">
                <h1 class="title text-uppercase m-0">conheça o </h1>
                <h1 class="subtitle text-uppercase m-0 pb-4">{{ $item->model->name }}</h1>
                <a href="#form" class="btn text-uppercase btn-primary rounded-0 border-0 pr-5 pl-5">Estou Interessado</a>
            </div>			
        </div>
        <div class="container p-5">
            <div class="content text-center p-4">
                <h1 class="text-title">
                   {{ $item->model->highlightsModel[1]->name }}
                   <br>
                   {{ $item->model->highlightsModel[1]->subtitle }}
                </h1>
            </div>
        </div>
        <div class="container" id="content">

            <x-HighlightsModel :item="$item->model" />
            <x-GallerysCars :item="$item->model->modelGallery" :name="$item->model->complete_name" />

            <div class="navs-tabs d-lg-none">
                <div class="owl-carousel owl-theme pb-5">
                    @foreach($item->model->highlightsModel as $key => $new)
                        @if($new->position !== 1 && $new->position !== 2 &&  $new->position !== 6)
                            <div class="item">
                                <div class="new">
                                    <div class="row">
                                        <div class="col-12 pb-2">
                                            <h2 class="text-right">{{ $new->name }}</h2>
                                        </div>
                                        <div class="col-12">
                                            <div class="bg" style="background-image: url('{{ $new->image }}')">
                                                <img src="{{ $new->image }}" class="sr-only" alt="{{ $new->name }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <h2 class="text-left">{{ $new->subtitle }}</h2>
                                            <div class="display">
                                                {!! $new->content !!}
                                            </div>
                                        </div>                                    
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach       
                </div>
                <div class="owl-carousel owl-theme">
                    @foreach($item->model->modelGallery as $key => $gallery)
                        <div class="item">
                            <img src="{{ $gallery->path }}" class="img-fluid" alt="{{ $item->model->complete_name }}">
                        </div>
                    @endforeach       
                </div>
            </div>

            @isset($item->model->highlightsModel[5])
                <div class="about-bottom navs-tabs">
                    <div class="row">
                        <div class="col-12 pb-5">
                            <h2 class="text-right">{{ $item->model->highlightsModel[5]->name }}</h2>
                        </div>
                        <div class="col-12 col-md-6 order-2 order-md-1">
                            <h2 class="text-left">{{ $item->model->highlightsModel[5]->subtitle }}</h2>
                            <div class="display">
                                {!! $item->model->highlightsModel[5]->content !!}
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-1 order-md-2">
                            <div class="bg" style="background-image: url('{{ $item->model->highlightsModel[5]->image }}')">
                                <img src="{{ $item->model->highlightsModel[5]->image }}" class="sr-only" alt="{{ $item->model->complete_name }}">
                            </div>
                        </div>
                    </div>
                </div>
            @endisset

            <x-Versions :item="$item->model->versions" :name="$item->model->complete_name"/>
        </div>				
    </section>
    <x-FormNews />
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({
                items: 1,
                nav: true,
                loop: true,
                dots: true,
                nav: false,
                lazyLoad:true,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true
            });
        })
    </script>
@endpush