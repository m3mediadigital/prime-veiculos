@extends('layouts.default', ['title' => 'Agendamento'])

@php
    $ano = [
        ['item' => '10.000km ou 1 ano'],
        ['item' => '20.000km ou 2 ano'],
        ['item' => '30.000km ou 3 ano'],
    ];
@endphp

@section('content')
      <section class="news offers pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offers-title text-center text-title mb-0 text-capitalize">agendamento</h1>
            <p class="text-center pb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. <br> Placeat minima voluptas aperiam optio distinctio 
                harum esse!</p>
            <form class="pt-1 forms pb-5">
                <div class="form-row">
                    <div class="form-group col-12 col-md-7">
                        @error('name') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <input type="text" class="form-control text-uppercase @error('name') is-invalid @enderror" placeholder=" " name="name" value="{{ old('name') }}">
                        <label for="name" class="text-uppercase">nome completo</label>                    
                    </div>
                    <div class="form-group col-12 col-md-5">
                        @error('phone') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <input type="tel" class="form-control text-uppercase sp_celphones @error('phone') is-invalid @enderror" placeholder=" " name="phone" value="{{ old('phone') }}">
                        <label for="name" class="text-uppercase">telefone</label>                    
                    </div>
                    <div class="form-group col-12 col-md-8">
                        @error('email') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <input type="email" class="form-control text-uppercase @error('email') is-invalid @enderror" placeholder=" " name="email" value="{{ old('email') }}">
                        <label for="name" class="text-uppercase">e-mail</label>                    
                    </div>
                    <div class="form-group col-12 col-md-4">
                        @error('loja') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <select class="form-control text-uppercase @error('loja') is-invalid @enderror" placeholder=" " name="loja">
                            <option value="">Loja</option>
                            <option value="{{ old('loja') }}">Loja</option>
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        @error('model') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <select class="form-control text-uppercase @error('model') is-invalid @enderror" placeholder=" " name="model">
                            <option value="">Modelo</option>
                            <option value="{{ old('model') }}">model</option>
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        @error('board') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                        <select class="form-control text-uppercase @error('board') is-invalid @enderror" placeholder=" " name="board">
                            <option value="">Placa</option>
                            <option value="{{ old('board') }}">Loja</option>
                        </select>
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="" class="text-uppercase"><strong>opção 1</strong></label>
                        <div class="form-row">
                            <div class="form-group col-12 col-md-6">
                                @error('dataone') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                                <select class="form-control text-uppercase @error('dataone') is-invalid @enderror" placeholder=" " name="dataone">
                                    <option value="">data</option>
                                    <option value="{{ old('dataone') }}">Loja</option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                @error('horsone') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                                <select class="form-control text-uppercase @error('horsone') is-invalid @enderror" placeholder=" " name="horsone">
                                    <option value="">hora</option>
                                    <option value="{{ old('horsone') }}">Loja</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="" class="text-uppercase"><strong>opção 2</strong></label>
                        <div class="form-row">
                            <div class="form-group col-12 col-md-6">
                                @error('datatwo') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                                <select class="form-control text-uppercase @error('datatwo') is-invalid @enderror" placeholder=" " name="datatwo">
                                    <option value="">data</option>
                                    <option value="{{ old('datatwo') }}">Loja</option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                @error('horstow') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                                <select class="form-control text-uppercase @error('horstow') is-invalid @enderror" placeholder=" " name="horstow">
                                    <option value="">hora</option>
                                    <option value="{{ old('horstow') }}">Loja</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <ul class="navbar-nav nav">
                            @foreach ($ano as $item)
                                <li class="nav-item pt-3">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input mr-2" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="{{ $item['item'] }}">
                                        <label class="form-check-label text-title" for="$item['item']">{{ $item['item'] }}</label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>                        
                    </div>
                    <div class="col-12 col-md-3">
                        <ul class="navbar-nav nav">
                            @foreach ($ano as $item)
                                <li class="nav-item pt-3">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input mr-2" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="{{ $item['item'] }}">
                                        <label class="form-check-label text-title" for="$item['item']">{{ $item['item'] }}</label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>                        
                    </div>
                    <div class="col-12 col-md-3">
                        <ul class="navbar-nav nav">
                            @foreach ($ano as $item)
                                <li class="nav-item pt-3">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input mr-2" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="{{ $item['item'] }}">
                                        <label class="form-check-label text-title" for="$item['item']">{{ $item['item'] }}</label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>                        
                    </div>
                    <div class="col-12 col-md-3">
                        <ul class="navbar-nav nav">
                            @foreach ($ano as $item)
                                <li class="nav-item pt-3">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input mr-2" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="{{ $item['item'] }}">
                                        <label class="form-check-label text-title" for="$item['item']">{{ $item['item'] }}</label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>                        
                    </div>
                </div>
                <div class="col-12 d-flex justify-content-center pt-5">
                    <button type="submit" class="btn btn-primary border-0 rounded-0 pr-5 pl-5 text-uppercase">enviar</button>
                </div>
            </form>
        </div>
    </section>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>
    <script>
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

        $('.sp_celphones').mask(SPMaskBehavior, spOptions);
    </script>
@endpush