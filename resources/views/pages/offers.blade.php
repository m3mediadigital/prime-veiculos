@extends('layouts.default', ['title' => 'Ofertas'])

@section('content')
    <section class="offers pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offer-title text-title mb-0">As nossas melhores</h1>
            <h1 class="offer-sutitle text-uppercase pb-5 text-subtitle"><strong>ofertas</strong></h1>
            <div class="row">
                @forelse ($offers as $item)
                    <x-CardOffer :item="$item" />
                @empty
                    <p>Não temos ofertas</p>
                @endforelse
            </div>
        </div>
    </section>
@endsection