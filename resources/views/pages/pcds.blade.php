@extends('layouts.default', ['title' => 'PCDS'])

@section('content')
    <section class="offers pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offer-title text-title mb-0">Ofertas para</h1>
            <h1 class="offer-sutitle text-uppercase pb-5 text-subtitle"><strong>PCD</strong></h1>
           
            <div class="row">
                @forelse ($pcds as $item)
                    <x-CardDirectSales :item="$item" />
                @empty
                    <p>Não temos ofertas</p>
                @endforelse
            </div>
        </div>
    </section>
@endsection