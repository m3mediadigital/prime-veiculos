@extends('layouts.default', ['title' => 'Seja Bem Vindo'])

@section('content')
    @include('sections._slide')
    @include('sections._offers')
    @include('sections._useds')
@endsection