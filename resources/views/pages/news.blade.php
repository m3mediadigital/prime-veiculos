@extends('layouts.default', ['title' => 'Showroom'])

@section('content')
    <section class="news offers pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offers-title text-center text-title mb-0">Novos</h1>
            <p class="text-center pb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. <br> Placeat minima voluptas aperiam optio distinctio 
                harum esse!</p>
            <div class="row">
                @forelse ($news as $item)
                    <x-CardNews :item="$item" />
                @empty
                    
                @endforelse
            </div>
        </div>
    </section>
@endsection