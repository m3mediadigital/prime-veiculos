@extends('layouts.default', ['title' => 'Seminovo'])

@section('content')
    <section class="useds pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offer-title text-title mb-0">Gol Bola G5</h1>
            <div class="row pt-4">
                {{-- @foreach($useds as $item)
                    <x-CardUseds :item="$item" />
                @endforeach --}}
                <div class="col-12 col-lg-6">
                    <img src="{{ asset('images/min/carro_user.jpg') }}" class="w-100" alt="">
                </div>
                <div class="col-12 col-lg-6">
                    <h3 class="text-subtitle">1.0 MPI 4P</h3>
                    <p class="text-title m-0 pt-4"><strong>Por apenas R$</strong></p>
                    <h1 class="text-subtitle"><strong>R$ 14.990<sup>,00</sup></strong></h1>
                </div>
                <div class="col-12 pt-5">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos exercitationem doloremque veritatis. Impedit cum unde, porro quia nobis mollitia quo possimus iure inventore laborum at praesentium dolores rem. Cumque, reprehenderit!</p>
                </div>
            </div>
        </div>
    </section>
    <x-FormNews />
@endsection