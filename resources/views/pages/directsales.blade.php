@extends('layouts.default', ['title' => 'Vendas Corporativas'])

@section('content')
    <section class="offers pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offer-title text-title mb-0">Ofertas para</h1>
            <h1 class="offer-sutitle text-uppercase pb-5 text-subtitle"><strong>venda direta</strong></h1>

           <x-FilterDirectSales :showroom="$directSales" />
           
            <div class="row">
                @forelse ($directSales as $item)
                    <x-CardDirectSales :item="$item" />
                @empty
                    <p>Não temos ofertas</p>
                @endforelse
            </div>
        </div>
    </section>
@endsection