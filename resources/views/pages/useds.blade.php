@extends('layouts.default', ['title' => 'Seminovos'])

@section('content')
    <section class="useds pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offer-title text-title mb-0">Os melhores</h1>
            <h1 class="offer-sutitle text-uppercase pb-5 text-subtitle"><strong>seminovos</strong></h1>
            <div class="row">
                @foreach($useds as $item)
                    <x-CardUseds :item="$item" />
                @endforeach
            </div>
        </div>
    </section>
@endsection