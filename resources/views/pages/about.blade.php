@extends('layouts.default', ['title' => 'Quem Somos'])
@php
    $items = [
        [
            'title' => 'conheça a discar 1',
            'content' => 'A Discar foi fundada em 1968 e em 1999 foi adquirida pelo Grupo Raimundo Juliano. São 50 anos realizando o sonho de consumo 
                        de milhares de clientes que adquirem seus veículos através da nossa empresa. Ao longo dos anos muitas foram as conquistas, 
                        procurando sempre a satisfação total de nossos clientes, modernizando instalações, treinando e valorizando funcionários, 
                        agregando novas tecnologias e diversificando serviços. Com um time de profissionais treinados e constantemente atualizados, 
                        a Discar se destaca pela qualidade no atendimento prestado aos nossos clientes. Em 2001 foi inaugurada a Discar Riomar, 
                        situada numa região nobre de Aracaju com instalações modernas e planejadas para oferecer conforto e funcionalidade. 
                        E em 2014 foi inaugurada a Discar Itabaiana com um showroom climatizado, dentro do novo padrão modular Volkswagen.'
        ],
        [
            'title' => 'conheça a discar 2',
            'content' => 'os clientes, modernizando instalações, treinando e valorizando funcionários, 
                        agregando novas tecnologias e diversificando serviços. Com um time de profissionais treinados e constantemente atualizados, 
                        a Discar se destaca pela qualidade no atendimento prestado aos nossos clientes. Em 2001 foi inaugurada a Discar Riomar, 
                        situada numa região nobre de Aracaju com instalações modernas e planejadas para oferecer conforto e funcionalidade. 
                        E em 2014 foi inaugurada a Discar Itabaiana com um showroom climatizado, dentro do novo padrão modular Volkswagen.'
        ],
        [
            'title' => 'conheça a discar 3',
            'content' => 'A Discar foi fundada em 1968 e em 1999 foi adquirida pelo Grupo Raimundo Juliano. São 50 anos realizando o sonho de consumo 
                        de milhares de clientes que adquirem seus veículos através da nossa empresa. Ao longo dos anos muitas foram as conquistas, 
                        procurando sempre a satisfação total de nossos clientes, modernizando instalações, treinando e valorizando funcionários, 
                        agregando novas tecnologias e diversificando serviços. Com um time de profissionais treinados e constantemente atualizados, 
                        a Discar se destaca pela qualidade no atendimento prestado aos nossos clientes. Em 2001 foi inaugurada a Discar Riomar, 
                        situada numa região nobre de Aracaju com instalações modernas e planejadas para oferecer conforto e funcionalidade. 
                        E em 2014 foi inaugurada a Discar Itabaiana com um showroom climatizado, dentro do novo padrão modular Volkswagen.'
        ]
    ];

    $images = [
        ['image' => 'galeria1.jpg'],
        ['image' => 'galeria2.jpg'],
        ['image' => 'galeria3.jpg']
    ];
@endphp
@section('content')
    <section class="about new offers pt-5 pb-5">
        <div class="container pt-5">
            <h1 class="offers-title text-center text-title mb-0">Discar</h1>
            <div class="pt-5 pb-0 versions">
                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                    @foreach ($items as $item)
                        <li class="nav-item" role="presentation">
                            <a class="nav-link text-capitalize {{ $loop->first ? ' active' : '' }}" id="pills-{{ Str::of($item['title'])->slug('-') }}-tab" data-toggle="pill" 
                                href="#{{ Str::of($item['title'])->slug('-') }}" role="tab" aria-controls="pills-home" aria-selected="{{ $loop->first ? true : false }}">
                                <strong>{{ $item['title'] }}</strong>
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content" id="pills-tabContent">
                    @foreach ($items as $item)
                        <div class="tab-pane fade{{ $loop->first ?' show active' : '' }}" id="{{ Str::of($item['title'])->slug('-') }}" role="tabpanel" 
                            aria-labelledby="pills-{{ Str::of($item['title'])->slug('-') }}-tab">
                            {!! $item['content'] !!}
                        </div>
                   @endforeach
                </div>
            </div>

            <div class="galery pt-5 pb-3">
                <div class="row">
                    @foreach($images as $key => $item)
                        <div class="pt-4
                            @php switch ($key){
                                case 0:
                                    echo 'col-12 col-md-4 primary';
                                    break;							
                                case 1:
                                    echo 'col-12 col-md-8';
                                    break;
                                case 2:
                                    echo 'col-12';
                                    break;
                                case 3:
                                    echo 'col-12 col-md-8';
                                    break;
                                case 4:
                                    echo 'col-12 col-md-4';
                                    break;
                                case 5:
                                    echo 'col-12 col-md-4';
                                    break;
                                case 6:
                                    echo 'col-12 col-md-4';
                                    break;
                                case 7:
                                    echo 'col-12 col-md-4';
                                    break;
                                case 8:
                                    echo 'col-12';
                                    break;
                                case 9:
                                    echo 'col-12 col-md-4';
                                    break;
                                case 10:
                                    echo 'col-12 col-md-8';
                                    break;
                                default:
                                    # code...
                                    break;
                            }
                            @endphp">
                            <div class="galery-img" style="background-image: url('{{ asset('images/min/'.$item['image']) }}')">
                                <img src="{{ asset('images/min/'.$item['image']) }}" class="img-fluid sr-only" alt="{{ env('APP_NAME') }}">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @include('sections._form_work_us')
@endsection