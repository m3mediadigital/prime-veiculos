<head>
    {{-- {!! @$settings['google-tag-manage-head'] !!}
    {!! @$settings['google-analitycs'] !!} --}}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--
        <meta name="description" content="{{ @$settings['descriacao-do-site'] }}">
        <meta name="keywords" content="{{ @$settings['keywords'] }}">
        <meta name="robots" content="index,follow,noodp">
        <meta name="googlebot" content="index,follow">
        <meta name="subjet" content="{{ @$settings['descriacao-do-site'] }}">
        <meta name="abstract" content="{{ @$settings['descriacao-do-site'] }}">
        <meta name="topic" content="{{ @$settings['descriacao-do-site'] }}">
        <meta name="summary" content="{{ @$settings['descriacao-do-site'] }}">
        <meta property="og:url" content="{{ request()->fullUrl() }}">
        <meta property="og:type" content="{{ env('OG_TYPE', 'website') }}">
        <meta property="og:title" content="{{ env('APP_NAME','Apform') }} {{ ' - '.@$title }}"> 
        <meta property="og:image" content="{{ asset('images/icons/favicon.svg') }}">
        <meta property="og:description" content="{{ @$settings['descriacao-do-site'] }}">
        <meta property="og:site_name" content="{{ env('APP_NAME','Apform') }} {{ ' - '.@$title }}">
        <meta property="og:locale" content="pt_BR">
        <meta itemprop="name" content="{{ env('APP_NAME','Apform') }} {{ ' - '.@$title }}">
        <meta itemprop="description" content="{{ @$settings['descriacao-do-site'] }}">
        <meta itemprop="image" content="{{ asset('images/icons/favicon.svg') }}">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="{{ env('APP_NAME','Apform') }} {{ ' - '.@$title }}">
        <meta name="twitter:url" content="{{ request()->fullUrl() }}">
        <meta name="twitter:title" content="{{ env('APP_NAME','Apform') }} {{ ' - '.@$title }}">
        <meta name="twitter:description" content="{{ @$settings['descriacao-do-site'] }}">
        <meta name="twitter:image" content="{{ asset('images/icons/favicon.svg') }}">
    --}}
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/icons/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/icons/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/icons/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/icons/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/icons/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/icons/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/icons/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/icons/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/icons/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/icons/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/icons/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/icons/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/icons/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('images/icons/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#001e50">
    <meta name="msapplication-TileImage" content="{{ asset('image/icons/favicon/ms-icon-144x144') }}">
    <meta name="theme-color" content="#001e50">

    <title>{{ config('app.name', 'NovaM3') .' - '. @$title }}</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    
    @stack('css')
</head>