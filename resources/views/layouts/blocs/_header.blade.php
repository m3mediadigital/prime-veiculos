<header class="header">
    <div class="header-primary position-fixed w-100">
        <div class="container-fluid">
            <ul class="nav justify-content-between">
                <li class="nav-item d-none d-md-block">
                    {{-- <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <p class="m-0">
                                    <strong>A+</strong>
                                    <strong>A-</strong>
                                </p>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <p class="m-0">
                                    <strong>A+</strong>
                                    <strong>A-</strong>
                                </p>
                            </a>
                        </li> 
                    </ul> --}}
                </li>       
                <li class="nav-item d-flex align-items-center">
                    <ul class="nav whatsapp justify-content-center">
                        @for($i = 0; $i < 2; $i++)
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <p class="m-0">
                                    <span class="text-capitalize">serviços</span>
                                    <img src="{{ asset('images/icons/whatsapp.svg') }}" class="mr-1 ml-1" alt="{{ env('APP_NAME') }}">
                                    <span>(79) 9 9957 8629</span>
                                </p>
                            </a>
                        </li>
                        @endfor
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-expand-md navbar-light pt-5">
        <div class="container-fluid">
            <div class="hamburger hamburger--squeeze js-hamburger d-lg-none" data-toggle="collapse" data-target="#navbarDesktop" aria-controls="navbarDesktop" aria-expanded="true">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            <a class="navbar-brand m-0 d-md-none" href="{{ route('index') }}">
                <img src="{{ asset('images/icons/logo.png') }}" alt="{{ env('APP_NAME') }}">
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item pr-5 d-none d-lg-block">
                        <div class="hamburger hamburger--squeeze js-hamburger position-absolute" data-toggle="collapse" data-target="#navbarDesktop" aria-controls="navbarDesktop" aria-expanded="false">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item pl-5">
                        <a class="nav-link{{ Route::is('index') ? ' active' :'' }}" href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ Route::is('news') ? ' active' :'' }}" href="{{ route('news') }}">Showroom</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ Route::is('offers') ? ' active' :'' }}" href="{{ route('offers') }}">Ofertas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ Route::is('useds') ? ' active' :'' }}" href="{{ route('useds') }}">Seminovos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ Route::is('scheduling') ? ' active' :'' }}" href="{{ route('scheduling') }}">Serviços</a>
                    </li>
                </ul>
                <a class="navbar-brand my-2 my-lg-0 d-none d-md-block" href="{{ route('index') }}" style="width: 150px">
                    <img src="{{ asset('images/icons/logo.png') }}" class="w-100" alt="{{ env('APP_NAME') }}">
                </a>
            </div>
        </div>
    </nav>
    <div class="collapse navbar-collapse w-100 position-fixed" id="navbarDesktop">
        <a class="navbar-brand m-0 position-absolute pb-5 pb-lg-4" href="{{ route('index') }}" style="width: 195px">
            <img src="{{ asset('images/icons/logo-white.png') }}" alt="{{ env('APP_NAME') }}" class="w-100">
        </a>
        <hr class="mb-0">
        <div class="row pt-lg-4">
            <div class="col-12 col-md-6 col-lg-5 p-0">
                <ul class="navbar-nav mr-auto pb-5">
                    <li class="nav-item pl-5">
                        <a class="nav-link active" href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('news') }}">Showroom</a>
                    </li>
                    <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('offers') }}">Ofertas</a>
                    </li>
                    <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('useds') }}">Seminovos</a>
                    </li>
                    <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('scheduling') }}">Serviços</a>
                    </li>
                    <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('about') }}">Quem Somos</a>
                    </li>
                    {{-- <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('directsales') }}">Vendas Corporativas</a>
                    </li> --}}
                    {{-- <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('pcds') }}">PCD</a>
                    </li> --}}
                    {{-- <li class="nav-item pl-5">
                        <a class="nav-link" href="#">Peças e Acessórios</a>
                    </li> --}}
                    <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('financing') }}">Financiamento</a>
                    </li>
                    {{-- <li class="nav-item pl-5">
                        <a class="nav-link" href="#">Consórcio</a>
                    </li> --}}
                    {{-- <li class="nav-item pl-5">
                        <a class="nav-link" href="#">Trabalhe Conosco</a>
                    </li> --}}
                    <li class="nav-item pl-5">
                        <a class="nav-link" href="{{ route('contact') }}">Contato</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-7 p-0 right-0 d-none d-lg-block">
                <div class="row w-100 p-0 ml-2">
                    <div class="col-12">
                        <ul class="navbar-nav mr-auto w-100">
                            <li class="nav-item">
                                <a class="nav-link text-blue" href="#"><strong>Unidades</strong></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-blue" href="#"><strong>Telefones</strong></a>
                            </li>
                        </ul>
                        <ul class="navbar-nav w-100 m-0 pb-5">
                            @foreach ($units as $unit)
                                <li class="nav-item pb-4">
                                    <p class="m-0"><strong>{{ $unit['nome'] }}</strong></p>
                                    <p class="m-0">{{ $unit['endereco'] }}</p>
                                </li>
                                <li class="nav-item pb-4">
                                    <p class="m-0 pt-4">{{ $unit['telefone'] }}</p>
                                </li>
                            @endforeach
                        </ul>
                        <ul class="navbar-nav icons w-100 m-0 pt-5 pb-5">
                            <li class="nav-item pt-3 pb-5">
                                <a href="{{ $settings['instagram'] }}">
                                    <img src="{{ asset('images/icons/instagram.svg') }}" alt="Instagram">
                                </a>
                            </li>
                             <li class="nav-item pt-3 pb-5">
                                <a href="{{ $settings['twitter'] }}">
                                    <img src="{{ asset('images/icons/twitter.svg') }}" alt="Twitter">
                                </a>
                            </li>
                            <li class="nav-item pt-3 pb-5">
                                <a href="{{ $settings['facebook'] }}">
                                    <img src="{{ asset('images/icons/facebook.svg') }}" class="pl-1" alt="Facebook">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>