<footer>
    <div class="footer-top pt-2">
        <div class="container">
            <ul class="nav justify-content-between">
                <li class="nav-item">
                    <img src="{{ asset('images/icons/logo-white.png') }}" alt="{{ env('APP_NAME') }}" class="mt-2" style="width: 120px">
                </li>
                <li class="nav-item d-flex align-items-center">
                    <ul class="navbar-nav icons w-100 m-0">
                        <li class="nav-item pt-3 pb-4">
                            <a href="{{ $settings['instagram'] }}">
                                <img src="{{ asset('images/icons/instagram.svg') }}" alt="Instagram">
                            </a>
                        </li>
                        <li class="nav-item pt-3 pb-4">
                            <a href="{{ $settings['twitter'] }}">
                                <img src="{{ asset('images/icons/twitter.svg') }}" alt="Twitter">
                            </a>
                        </li>
                        <li class="nav-item pt-3 pb-4">
                            <a href="{{ $settings['facebook'] }}">
                                <img src="{{ asset('images/icons/facebook.svg') }}" class="pl-1" alt="Facebook">
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="footer-center pt-4 pb-4">
        <div class="container">
            <ul class="nav menu pb-2">
                <li class="nav-item" style="width:35%">
                    <a class="nav-link text-blue pl-0" href="#"><strong>Unidades</strong></a>
                </li>
                <li class="nav-item d-none d-lg-block" style="width: 20%">
                    <a class="nav-link text-blue" href="#"><strong>Telefones</strong></a>
                </li>
                <li class="nav-item d-none d-lg-block" style="width: 45%">
                    <a class="nav-link text-blue" href="#"><strong>Funcionamento</strong></a>
                </li>
            </ul>

            @foreach ($units as $unit)
                <ul class="nav info justify-content-lg-between d-block d-lg-flex">
                    <li class="nav-item pt-2">
                        <p class="m-0"><strong>{{ $unit['nome'] }}</strong></p>
                        <p class="m-0">{{ $unit['endereco'] }}</p>
                    </li>
                    <li class="nav-item pt-2 d-none d-lg-block">
                        <p class="m-0 pt-4">{{ $unit['telefone'] }}</p>
                    </li>
                    <li class="nav-item pt-2 d-none d-lg-block">
                        {!! $unit['horarios'] !!}
                    </li>
                </ul>
                <hr class="w-100">
            @endforeach

        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p class="pb-4 pt-4">Os valores informados por nossas mídias digitais podem sofrer variações decorrentes de:
                Data do fechamento do negócio; Acessórios instalados; Cor do veículo; Disponibilidade de estoque; Ano /
                Modelo; Especificações dos pedidos. Estando sujeita a confirmação mediante o comparecimento pessoal na
                loja. Informações de preços, taxas e prazos válidos apenas para o dia do contato.</p>
        </div>
    </div>
    <div class="container pb-5 pt-5 pt-lg-0 pb-lg-0">
        <ul class="nav footer align-items-center justify-content-lg-between w-100 m-0">
            <li class="nav-item text-center">
                <a href="">
                    © {{ now()->format('Y') }} Discar Volkswagen. Todos os diretos reservados.
                </a>
            </li>
            <li class="nav-item text-center">
                <a href="" class="text-title">
                    <strong>Política de privacidade</strong>
                </a>
            </li>
            <li class="nav-item pt-3 pb-5 d-none d-lg-block">
                <a href="https://novam3.com.br">
                    <img src="{{ asset('images/icons/logo_m3.svg') }}" class="pl-1" alt="Nova M3">
                </a>
            </li>
        </ul>
    </div>
</footer>
