<!DOCTYPE html>
<html lang="en">
    @include('layouts.blocs._head')
    <body>
        @include('layouts.blocs._header')
            @yield('content')
        @include('layouts.blocs._footer')
        @include('layouts.blocs._script')
    </body>
</html>