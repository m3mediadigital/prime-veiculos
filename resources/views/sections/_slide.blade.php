<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @foreach ($slides as $item)
            <figure class="carousel-item{{ $loop->first ? ' active' : '' }}" data-interval="5000">
                <a href="{{ $item->link ?? 'javascript;;' }}" target="_blank" rel="noopener noreferrer">
                    <img data-src="{{ $item->image }}" class="d-block w-100 lazy" alt="Slide Show">
                </a>
            </figure>
        @endforeach        
    </div>
    <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div class="more p-5 d-flex justify-content-center">
    {{-- <p class="">ver mais <hr></p> --}}
    <ul class="nav">
        <li class="nav-item text-uppercase pr-2">
           ver mais
        </li>
        <li class="nav-item align-content-center pr-1">
            <hr>
        </li>
        <li class="nav-item">
             <img src="{{ asset('images/icons/arrow-down.svg') }}" alt="{{ env('APP_NAME') }}">
        </li>
    </ul>
</div>