<section class="offers pt-5 pb-5">
    <div class="container">
        <h1 class="offer-title text-title mb-0">Você não pode</h1>
        <h1 class="offer-sutitle text-uppercase pb-5 text-subtitle"><strong>perder</strong></h1>
        <div class="row d-flex justify-content-end">
            <a href="{{ route('offers') }}" class="text-subtitle offers-more">Ver todas as ofertas <img src="{{ asset('images/icons/arrow-right.svg') }}" class="ml-2" alt="{{ env('APP_NAME') }}"> </a>
        </div>
        <div class="row">
            @forelse ($offers as $item)
                 <x-CardOffer :item="$item" />
            @empty
                <p>Não temos ofertas</p>
            @endforelse
        </div>
    </div>
</section>