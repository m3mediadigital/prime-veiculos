<section class="useds pt-5">
    <div class="container">
        <img src="{{ asset('images/min/banner_useds.jpg') }}" class="pb-5 img-fluid" alt="{{ env('APP_NAME') }}">
        <div class="row">
            @foreach($useds as $item)
                <x-CardUseds :item="$item" />
            @endforeach
        </div>
    </div>
</section>