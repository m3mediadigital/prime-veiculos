<div class="modal-ofertas modal fade" id="serial-modal{{ $item->slug }}" tabindex="-1" role="dialog" aria-labelledby="{{ $item->slug }}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ $item->complete_name }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="checkmark">
                    @foreach($item->optionals as $key => $optinal)
                        <li>{{ $optinal->value }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>