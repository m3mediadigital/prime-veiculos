<div class="col-12 col-md-6 col-lg-4 pb-5 pt-4">
    <a href="{{ route('new',['slug' => $item->slug]) }}" class="offers-card pb-5">
        <div class="card border-0 w-100">
            <div class="card-body pb-5">
                <h5 class="text-title text-center pb-5 text-capitalize m-0 pt-3"><strong>{{ $item->model->complete_name }}</strong></h5>
            </div>
            <figure class="card-footer text-center" style="margin-top: -95px;">
                <img data-src="{{ $item->model->image }}" class="card-img-top w-100 lazy" alt="{{ $item->model->complete_name }}">
            </figure>
        </div>
    </a>
</div>