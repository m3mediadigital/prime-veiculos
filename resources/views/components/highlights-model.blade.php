<div class="navs-tabs pb-5 d-none d-lg-block">
    <div class="row">
        <div class="col-5">
            <div class="tab-content" id="v-pills-tabContent">
                @for($i = 2; $i < $count; $i++)
                    <div class="tab-pane fade{{ $i == 2 ? ' show active' : '' }}" id="v-pills-home-{{ $item->highlightsModel[$i]->id }}" role="tabpanel" 
                        aria-labelledby="v-pills-home-tab-{{ $item->highlightsModel[$i]->id }}" style="{{ $i == 2 ? 'opacity: 1;' : '' }}">
                        <h2>{{ $item->highlightsModel[$i]->name }}</h2>
                        <div class="bg" style="background-image: url('{{ $item->highlightsModel[$i]->image }}')">
                            <img src="{{ $item->highlightsModel[$i]->image }}" class="img-fluid sr-only" alt="{{ $item->highlightsModel[$i]->name }}">
                        </div>
                    </div>
                @endfor
            </div>
        </div>
        <div class="col-7">
                <div class="nav flex-column nav-pills" id="v-pills-tabContent" role="tablist" aria-orientation="vertical">
                @for($i = 2; $i < $count; $i++)
                    <a class="nav-link{{  $i === 2 ? ' active' : ''  }}" id="v-pills-home-tab-{{ $item->highlightsModel[$i]->id }}" data-toggle="pill" 
                        href="#v-pills-home-{{ $item->highlightsModel[$i]->id }}" role="tab" 
                        aria-controls="v-pills-home-<{{ $item->highlightsModel[$i]->id }}" aria-selected="{{  $i == 2 ? true : false  }}">
                        <h2 class="title">{{ $item->highlightsModel[$i]->name }}<span></span></h2>
                        <div class="display fade">
                            {!! $item->highlightsModel[$i]->content !!}
                        </div>
                    </a>
                @endfor
            </div>
        </div>
    </div>
</div>