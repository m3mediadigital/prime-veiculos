<div class="galery pb-3 d-none d-lg-block">
    <div class="row">
        @foreach($item as $key => $gallery)
            <div class="pt-4 @php switch ($key) {
                case 0:
                    echo 'col-12 col-md-4 primary';
                    break;							
                case 1:
                    echo 'col-12 col-md-8';
                    break;
                case 2:
                    echo 'col-12';
                    break;
                case 3:
                    echo 'col-12 col-md-8';
                    break;
                case 4:
                    echo 'col-12 col-md-4';
                    break;
                case 5:
                    echo 'col-12 col-md-4';
                    break;
                case 6:
                    echo 'col-12 col-md-4';
                    break;
                case 7:
                    echo 'col-12 col-md-4';
                    break;
                case 8:
                    echo 'col-12';
                    break;
                case 9:
                    echo 'col-12 col-md-4';
                    break;
                case 10:
                    echo 'col-12 col-md-8';
                    break;
                default:
                    # code...
                    break;
            } @endphp">
                <div class="galery-img" style="background-image: url('{{ $gallery->path }}')">
                    <img src="{{ $gallery->path }}" class="img-fluid sr-only" alt="{{ $name }}">
                </div>
            </div>
        @endforeach
    </div>
</div>