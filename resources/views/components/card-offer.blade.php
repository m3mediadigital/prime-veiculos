<div class="col-12 col-md-6 col-lg-4 pb-5 pt-4">
    <a href="{{ route('offer', ['slug' => $item->slug]) }}" class="offers-card pb-5">
        <div class="card border-0 w-100">
            <ul class="nav pl-3">
                <x-LabelCategory :category="$item->corporate_sales_category" />
                @if (isset($item->widget))
                    <x-LabelWidget :widgets="$item->widget" />
                @else
                    <li class="nav-item pl-2 pr-2 mr-2 text-uppercase label-widget"
                        style="color: transparent; background: transparent">
                        Default
                    </li>
                @endif
            </ul>
            <div class="card-body">
                <h5 class="text-title text-capitalize m-0 pt-3"><strong>{{ $item->model->name }}</strong></h5>
                <p class="card-text text-subtitle text-uppercase d-flex align-items-center">
                    {{ $item->model->year . ' ' . $item->version->complete_name }}
                </p>
                <x-Price :typePrice="$typePrice" :values="$priceValues" />
            </div>
            <figure class="card-footer text-center">
                <img data-src="{{ $item->color !== null ? $item->color->image : $item->version->large_image }}"
                    class="card-img-top w-100 lazy" alt="{{ $item->model->name }}">
            </figure>
        </div>
    </a>
</div>
