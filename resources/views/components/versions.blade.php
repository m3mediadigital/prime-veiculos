<div class="versions">
    <h1 class="text-center">Confira as versões disponíveis</h1>
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        @foreach ($item as $key => $version)
            <li class="nav-item " role="presentation">
                <a class="nav-link{{ $loop->first ? ' active' : '' }}" id="pills-{{ $version->slug }}"
                    data-toggle="pill" href="#v-{{ $version->slug }}" role="tab" aria-controls="pills-home"
                    aria-selected="true">
                    {{ $version->name }}
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content" id="pills-tabContent">
        @foreach ($item as $key => $version)
            <div class="tab-pane fade{{ $loop->first ? ' show active' : '' }}" id="v-{{ $version->slug }}"
                role="tabpanel" aria-labelledby="pills-{{ $version->slug }}"
                {{ $loop->first ? ' style="opacity: 1;"' : '' }}>
                <div class="row">
                    @isset($version->colors)
                        <div class="col-12 col-md-1 order-2 order-md-1">
                            <div class="cores">
                                <p class="text-uppercase">cores</p>
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                    aria-orientation="vertical">
                                    @foreach ($version->colors as $key => $color)
                                        <a class="nav-link{{ $loop->first ? ' active' : '' }}"
                                            id="v-pills-home-tab{{ $color->id }}" data-toggle="pill"
                                            href="#v-pills-{{ $color->id }}" role="tab" aria-controls="v-pills-home"
                                            aria-selected="true" title="{{ $color->color }}"
                                            style="background:{{ $color->hexadecimal }}"></a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 order-1 order-md-2">
                            <div class="tab-content" id="v-pills-tabContent">
                                @foreach ($version->colors as $key => $color)
                                    <div class="tab-pane fade{{ $loop->first ? ' show active' : '' }}"
                                        id="v-pills-{{ $color->id }}" role="tabpanel"
                                        aria-labelledby="v-pills-home-tab{{ $color->id }}"
                                        {{ $loop->first ? ' style="opacity: 1;"' : '' }}>

                                        {{-- <img src="= $carros->image ?>"
                                            class="img-fluid" alt="= $carros->complete_name ?>">
                                        --}}
                                        <div class="border-foto"
                                            style="background-image: url('{{ $color->image }}');  height: 26rem;">
                                            <img src="{{ $color->image }}" class="img-fluid sr-only" alt="{{ $name }}">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endisset
                    <div class="col-12 col-md-4 order-3">
                        <h2>{{ $version->complete_name }}</h2>
                        <div class="d-flex align-items-center">
                            <ul class="checkmark">
                                @foreach ($version->optionals as $key => $optinal)
                                    @if ($key <= 5)
                                        <li>{{ $optinal->value }}</li>
                                    @endif
                                @endforeach
                                <a href="#" data-toggle="modal" data-target="#serial-modal{{ $version->slug }}"
                                    class="mais-desconto-button"
                                    style="font-size: 13px;text-align: center;display:block;">ver mais</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <x-ModalOpcionals :item="$version" />
        @endforeach
    </div>
</div>
