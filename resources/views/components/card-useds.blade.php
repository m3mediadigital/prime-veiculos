<div class="col-12 col-md-6 col-lg-4 pb-5 pt-4">
    <div class="offers-card pb-5">
        <ul class="nav pl-3">
            <li class="nav-item pl-2 pr-2 mr-2">
                {{ $item->fab_year }} / {{ $item->model_year }}
            </li>
        </ul>
        <div class="card border-0 w-100">
            <img data-src="{{ $item->imagem_frontal }}" class="card-img-top rounded-0 w-100 lazy" alt="{{ env('APP_NAME') }}">
            <div class="card-body">
                <h5 class="text-title text-capitalize m-0"><strong>{{ $item->model->complete_name }}</strong></h5>
                <p class="card-text text-subtitle text-uppercase">{{ $item->version->complete_name }}</p>
                <x-Price :typePrice="$typePrice" :values="$priceValues" />
            </div>
            <a href="{{ route('used') }}" class="btn btn-primary rounded-0 p-2">Tenho interesse</a>
        </div>
    </div>
</div>