@if (isset($valor) && !is_null($valor))
    <p class="text-title m-0"><strong>Por apenas R$</strong></p>
    <h1 class="text-subtitle"><strong>{{$valor[0]}}<sup>,{{ $valor[1] }}</sup></strong></h1>
@endif
