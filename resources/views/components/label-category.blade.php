@if (isset($category) && !empty($category))
    <li class="nav-item pl-2 pr-2 mr-2 text-uppercase label-blue-light label-category">
        {{ str_replace('_',' ',$category) }}
    </li>
@endif
