@if (isset($widgets) && !empty($widgets))
    @foreach ($widgets as $k => $widget)
        <li class="nav-item pl-2 pr-2 mr-2 text-uppercase label-widget ">
            {{ $widget }}
        </li>
    @endforeach
@endif
