@if (isset($valores) && !is_null($valores))
    <p class="text-title m-0">Entrada ZERO {{$valores['parcel_amount']}}x de R$ </strong></p>
    <h1 class="text-subtitle">
        <strong>
            {{ $valores['parcel_value'][0] }}
            <sup>,{{ $valores['parcel_value'][1] }}</sup>
        </strong>
    </h1>
@endif