@if (isset($valores) && !is_null($valores))
    <p class="text-title m-0"><strong>De R$ {{$valores['value_from'][0]}}<sup>,{{ $valores['value_from'][1] }}</sup> Por R$ </strong></p>
    <h1 class="text-subtitle"><strong>{{$valores['value_to'][0]}}<sup>,{{ $valores['value_to'][1] }}</sup></strong></h1>
@endif