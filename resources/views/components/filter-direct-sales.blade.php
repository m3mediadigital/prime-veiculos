<div class="row d-flex justify-content-end offers-more pb-3">
    <select class="form-control w-25 text-uppercase">
        <option value="">Filtrar</option>
        @foreach ($showroom as $item)
            <option value="{{ $item }}">{{ $item }}</option>
        @endforeach
    </select>
</div>