@if (isset($price) && !is_null($price))
    <p class="text-title m-0"><strong>Bônus de R$ </strong></p>
    <h1 class="text-subtitle"><strong>{{$price[0]}}<sup>,{{ $price[1] }}</sup></strong></h1>
@endif
