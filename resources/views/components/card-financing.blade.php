<div class="col-12 col-md-6 col-lg-4 pb-5 pt-4">
    <a href="{{ route('news') }}" class="offers-card pb-5">
        <div class="card border-0 w-100">
            <div class="card-body pb-5">
                <h5 class="text-title text-capitalize m-0 pt-3"><strong>nivus comfortline</strong></h5>
                <p class="card-text text-subtitle text-uppercase pb-5">2020 2.0 flex aut.</p>
            </div>
            <figure class="card-footer text-center" style="margin-top: -95px;">
                <img data-src="{{asset('images/min/carro.jpg')}}" class="card-img-top w-100 lazy" alt="{{ env('APP_NAME') }}">
            </figure>
        </div>
    </a>
</div>