<section class="forms offers new pb-5">
    <div class="container pb-5">
        <h1 class="offers-title text-center text-title mb-0 text-capitalize pb-5"><strong>faça uma cotação<strong></h1>
        <form class="pt-1">
            <div class="form-row">
                <div class="form-group col-12 col-md-7">
                    @error('name') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                    <input type="text" class="form-control text-uppercase @error('name') is-invalid @enderror" placeholder=" " name="name" value="{{ old('name') }}">
                    <label for="name" class="text-uppercase">nome completo</label>                    
                </div>
                <div class="form-group col-12 col-md-5">
                    @error('phone') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                    <input type="tel" class="form-control text-uppercase sp_celphones @error('phone') is-invalid @enderror" placeholder=" " name="phone" value="{{ old('phone') }}">
                    <label for="name" class="text-uppercase">telefone</label>                    
                </div>
                <div class="form-group col-12 col-md-6">
                    @error('email') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                    <input type="email" class="form-control text-uppercase @error('email') is-invalid @enderror" placeholder=" " name="email" value="{{ old('email') }}">
                    <label for="name" class="text-uppercase">e-mail</label>                    
                </div>
                {{-- <div class="form-group col-12 col-md-3">
                    @error('loja') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                    <select class="form-control text-uppercase @error('loja') is-invalid @enderror" placeholder=" " name="loja">
                        <option value="">Loja</option>
                        <option value="{{ old('loja') }}">Loja</option>
                    </select>
                </div> --}}
                <div class="form-group col-12 col-md-3">
                    @error('financing') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                    <select class="form-control text-uppercase @error('financing') is-invalid @enderror" placeholder=" " name="financing">
                        <option value="">deseja financiar?</option>
                        <option value="{{ old('financing') }}">sim</option>
                    </select>
                </div>
                <div class="form-group col-12 col-md-3">
                    @error('input') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                    <input type="text" class="form-control money text-uppercase @error('input') is-invalid @enderror" placeholder=" " name="input" value="{{ old('input') }}">
                    <label for="name" class="text-uppercase">entrada</label> 
                </div>
                <div class="form-group col-12 col-md-4">
                    @error('parcel') <small class="form-text text-danger"><strong>{!! $message !!}</strong></small> @enderror
                    <select class="form-control text-uppercase @error('parcel') is-invalid @enderror" placeholder=" " name="parcel">
                        <option value="">parcelas</option>
                        <option value="{{ old('parcel') }}">sim</option>
                    </select>
                </div>
                <div class="form-grounp col-12 col-md-8 d-flex justify-content-center justify-content-lg-end">
                    <button type="submit" class="btn btn-primary border-0 rounded-0 pr-5 pl-5 btn-height text-uppercase">enviar</button>
                </div>
            </div>
        </form>
    </div>
</section>
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>
    <script>
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

        $('.sp_celphones').mask(SPMaskBehavior, spOptions);
        $('.money').mask('R$ 000.000,00', {reverse: true});
    </script>
@endpush