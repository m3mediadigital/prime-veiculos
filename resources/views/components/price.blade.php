@switch($typePrice)
    @case('Expressao')
        <x-PrecoExpressao :valores="$values" />
        @break
    @case('DescontoDe')
        <x-PrecoDescontoDe :valor="$values" />
        @break
    @case('ApenasPor')
        <x-PrecoPorApenas :valor="$values" />
        @break
    @case('APartirDe')
        <x-PrecoAPartirDe :valor="$values" />
        @break
    @case('BonusDe')
        <x-PrecoBonusDe :valor="$values" />
        @break
    @case('DePor')
        <x-PrecoDePor :valores="$values" />
        @break
    @case('EntradaParcelas')
        <x-PrecoEntradaParcelas :valores="$values" />
        @break
    @case('EntradaZero')
        <x-PrecoEntradaZero :valores="$values" />
        @break
    @default
        <x-PrecoSobConsulta />
        @break
@endswitch


